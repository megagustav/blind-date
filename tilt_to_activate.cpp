/*
   tilt to activate the green led (and the game)
*/


int tiltSensor = 2;
int greenLed = 12;
int redLed = 11;
int tiltSensorPreviousValue = 0;
int tiltSensorCurrentValue = 0;
long lastTimeTilted = 0; // how long it’s been since the state of the tilt sensor changed
int tiltTime = 500; // predefined time of tilting

void setup()
{
  pinMode(tiltSensor, INPUT);
  digitalWrite(tiltSensor, HIGH);
  pinMode(greenLed, OUTPUT);
  pinMode(redLed, OUTPUT);
}

void loop()
{
  tiltSensorCurrentValue = digitalRead(tiltSensor); // reading current value of tilt sensor
  if(tiltSensorPreviousValue != tiltSensorCurrentValue) // if tilted define time of change
  {
       lastTimeTilted = millis(); // returns the number of milliseconds passed
       tiltSensorPreviousValue = tiltSensorCurrentValue;
	 }
  if(millis() - lastTimeTilted < tiltTime) // if tilted more than 'shakeTime'
  {
       digitalWrite(greenLed, HIGH); // turn green led on
       digitalWrite(redLed, LOW);    // turn red led off
	 }
  else
  {
       digitalWrite(greenLed, LOW);  // turn green led off
       digitalWrite(redLed, HIGH);   // turn red led on
  }
}